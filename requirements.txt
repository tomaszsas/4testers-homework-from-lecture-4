argcomplete==3.2.2
click==8.1.7
colorama==0.4.6
iniconfig==2.0.0
packaging==23.2
pipx==1.4.3
platformdirs==4.2.0
pluggy==1.4.0
pytest==8.1.1
userpath==1.9.1
