import pytest

from src import homework_3

# Data for tests
list_of_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 'James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
female_fnames_with_error_type = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 15]
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def test_of_getting_one_element_from_a_list():
    assert homework_3.get_random_element_from_list(list_of_names) in list_of_names


def test_generate_email_address_from_fname_and_lname():
    fname = "Tomasz"
    lname = "Antos"
    assert homework_3.generate_email_address_from_fname_and_lname(fname, lname) == "tomasz.antos@example.com"


def test_generate_random_number_from_5_to_45():
    assert homework_3.generate_random_number_from_5_to_45() <= 45 and homework_3.generate_random_number_from_5_to_45() >= 5


def test_correct_keys_in_generated_dict_of_user_data_when_one_element_list():
    expected_keys = ['firstname', 'lastname', 'email', 'age', 'country', 'adult', 'birth_year']
    result = homework_3.generate_dict_of_user_data(["Tomasz"], ["Leszczyna"], ["Poland"])
    assert all(k in result.keys() for k in expected_keys)


def test_return_adult_false_if_user_age_less_than_18():
    while True:
        function_result = homework_3.generate_dict_of_user_data(list_of_names, surnames, countries)
        if function_result["age"] < 18:
            break
    assert not function_result["adult"]


def test_generate_10_elements_list_of_dict_users_data_return_proper_length_list():
    func_result = homework_3.generate_10_elements_list_of_dict_users_data(female_fnames, male_fnames, surnames, countries)
    assert len(func_result) == 10


def test_generate_10_elements_list_of_dict_users_data_return_5_male():
    users = homework_3.generate_10_elements_list_of_dict_users_data(female_fnames, male_fnames, surnames, countries)
    male_users = []
    for user in users:
        if user["firstname"] in male_fnames:
            male_users.append(user)
    assert len(male_users) == 5


def test_validate_data_for_generate_user_dict_raise_an_value_error():
    with pytest.raises(ValueError):
        homework_3.validate_data_for_generate_user_dict(female_fnames_with_error_type, surnames, countries)


