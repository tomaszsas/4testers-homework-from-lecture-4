import pytest

from src import employee_management

# Data for tests
list_of_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 'James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
list_of_surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
list_of_boolean = [True, False]

two_elements_list_of_names = ["Kamil", "Tomasz"]
two_elements_list_of_surname = ["Zdun", "Piec"]
four_elements_list_of_possible_emails = ["kamil.zdun@gmail.com", "tomasz.zdun@gmail.com", "kamil.piec@gmail.com", "tomasz.piec@gmail.com"]


def test_generate_random_email_address():
    domain = "gmail.com"
    assert employee_management.generate_random_email_address(two_elements_list_of_names, two_elements_list_of_surname, domain) in four_elements_list_of_possible_emails


def test_generate_random_number_from_1_to_50():
    generate_number = employee_management.generate_random_number_from_1_to_50()
    assert 1 <= generate_number <= 50


@pytest.mark.parametrize("different_lists", [list_of_names, list_of_boolean])
def test_get_random_element_from_list(different_lists):
    func_result = employee_management.get_random_element_from_list(different_lists)
    assert func_result in different_lists


def test_generate_employee_dict_return_all_correct_keys_names():
    expected_keys = ["email", "seniority_years", "female"]
    generate_employee_dict_result = list(employee_management.generate_employee_dict().keys())
    print(type(generate_employee_dict_result))
    assert all(k in generate_employee_dict_result for k in expected_keys)
