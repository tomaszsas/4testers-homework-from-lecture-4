# Jeśli wiek samochodu jest większy niż 5 lat lub przebieg jest większy niż 60 000
# kilometrów - > funkcja zwraca False. W przeciwnym wypadku zwraca True.
# Przetestuj funkcję dla samochodów z danymi:
# ● rocznik: 2020, przebieg 30 000
# ● rocznik: 2020, przebieg 70 000
# ● rocznik: 2016, przebieg 30 000
# ● rocznik: 2016, przebieg 120 000
import pytest

from src.homework_1 import verify_car_has_a_warranty


# Tests for is_car_has_a_warranty()
def test_car_from_2020_with_30_000_mileage_should_return_true():
    assert verify_car_has_a_warranty(2020, 30_000)


def test_car_from_2020_with_70_000_mileage_should_return_false():
    assert not verify_car_has_a_warranty(2020, 70_000)


def test_car_from_2016_with_30_000_mileage_should_return_false():
    assert not verify_car_has_a_warranty(2016, 30_000)


def test_car_from_2016_with_120_000_mileage_should_return_false():
    assert not verify_car_has_a_warranty(2016, 120_000)


def test_car_with_production_year_as_float_raise_a_type_error():
    with pytest.raises(TypeError):
        verify_car_has_a_warranty(2016.5, 60_000)
