import random


def generate_random_email_address(names: list, surnames: list, domain: str = "example.com") -> str:
    name = get_random_element_from_list(names)
    surname = get_random_element_from_list(surnames)
    return f"{name.lower()}.{surname.lower()}@{domain}"


def generate_random_number_from_1_to_50() -> int:
    return random.randint(1, 50)


def get_random_element_from_list(list_of_elements: list) -> str:
    return random.choice(list_of_elements)


def generate_employee_dict() -> dict:
    names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 'James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    employee = {
        "email": generate_random_email_address(names, surnames),
        "seniority_years": generate_random_number_from_1_to_50(),
        "female": get_random_element_from_list([True, False])
    }

    return employee


def generate_random_list_of_employees_dict(demand_list_length: int) -> list:
    list_of_employees = []
    for _ in range(demand_list_length):
        list_of_employees.append(generate_employee_dict())

    return list_of_employees


def get_employees_with_seniority_more_ten(list_of_employees: list) -> list:
    s_employees = []
    for employee in list_of_employees:
        if employee["seniority_years"] > 10:
            s_employees.append(employee)
    return s_employees


def get_female_employees_list(employees: list) -> list:
    female_employees = []
    for employee in employees:
        if employee["female"] is True:
            female_employees.append(employee)
    return female_employees


if __name__ == '__main__':
    # Lists with data for the draw
    employee_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 'James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    employee_surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']

    for i in range(5):
        random_employee = generate_random_email_address(employee_names, employee_surnames)
        print(random_employee)

    #  Generate list of dict
    employees_list = generate_random_list_of_employees_dict(20)
    print(employees_list)

    #  Get employees with seniority > 10
    senior_employees = get_employees_with_seniority_more_ten(employees_list)
    print(senior_employees)

    #  Get senior female employees
    female_senior_employees = get_female_employees_list(senior_employees)
    print(female_senior_employees)
