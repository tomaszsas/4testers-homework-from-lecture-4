# Napisz funkcję, która przyjmie rocznik i przebieg samochodu i zwróci czy samochód
# nadal ma gwarancję.
# Jeśli wiek samochodu jest większy niż 5 lat lub przebieg jest większy niż 60 000
# kilometrów - > funkcja zwraca False. W przeciwnym wypadku zwraca True.
# Przetestuj funkcję dla samochodów z danymi:
# ● rocznik: 2020, przebieg 30 000
# ● rocznik: 2020, przebieg 70 000
# ● rocznik: 2016, przebieg 30 000
# ● rocznik: 2016, przebieg 120 000

from datetime import date


def validate_numbers_are_integers(production_date, milage):
    if not all(isinstance(number, int) for number in [production_date, milage]):
        raise TypeError('number is not an integer')


def verify_car_has_a_warranty(production_date: int, milage: int) -> bool:
    validate_numbers_are_integers(production_date, milage)
    if date.today().year - production_date > 5 or milage > 60_000:
        return False
    else:
        return True
