# Skrypt zadanie.py zawiera listy danych:
# imiona żeńskie
# imiona męskie
# nazwiska
# nazwy krajów
# Poniżej umieszczone są dwa przykłady jak za pomocą funkcji random.choice() i
# random.randint() można wylosować przypadkowy element z listy lub zakresu liczb.
# Korzystając z tych danych wygeneruj listę słowników, która będzie zawierać 10
# elementów. Wymagania:
# 1. Każdy ze słowników ma zawierać klucze:
# firstname -> imię wylosowane z listy imion
# lastname -> nazwisko wylosowane z listy nazwisk
# country -> kraj wylosowany z listy krajów
# email -> postaci: imie.nazwisko@example.com (wszystko małymi literami)
# age -> wiek wylosowany z zakresu 5-45 lat
# adult -> o wartości True jeśli age >= 18 i False w przeciwnym
# przypadku
# birth_year -> rok urodzenia (obliczony na podstawie wylowoanego wieku)
# 2. W liście ma znajdować się po 5 imion męskich i 5 żeńskich (łącznie 10
# elementów)
# 3. Mając utworzoną listę słowników, dla każdego jej elementu proszę o
# wygenerowanie przedstawienia danej osoby w postaci następującego opisu
# (wartości pisane z dużych liter należy zastąpić kluczami ze słownika):
# Hi! I'm FIRSTNAME LASTNAME. I come from COUNTRY and I was born in BIRTH_YEAR

import random
from datetime import date


def get_random_element_from_list(list_of_elements: list) -> str:
    return random.choice(list_of_elements)


def generate_email_address_from_fname_and_lname(fname: str, lname: str, domain='example.com') -> str:
    return f"{fname.lower()}.{lname.lower()}@{domain}"


def generate_random_number_from_5_to_45() -> int:
    return random.randint(5, 45)


def is_object_list_of_string(obj: any) -> bool:
    return all(isinstance(elem, str) for elem in obj)


def validate_data_for_generate_user_dict(fnames_list: list, lnames_list: list, countries_list: list) -> None:
    for parameter_values in locals().values():
        if not is_object_list_of_string(parameter_values):
            raise ValueError("all elements in a list must be a string")


def generate_dict_of_user_data(fnames_list: list, lnames_list: list, countries_list: list) -> dict:
    validate_data_for_generate_user_dict(fnames_list, lnames_list, countries_list)
    user_fname = get_random_element_from_list(fnames_list)
    user_lname = get_random_element_from_list(lnames_list)
    user_age = generate_random_number_from_5_to_45()
    user_dict = {
        'firstname': user_fname,
        'lastname': user_lname,
        'email': generate_email_address_from_fname_and_lname(user_fname, user_lname),
        'age': user_age,
        'country': get_random_element_from_list(countries_list),
        'adult': True if user_age >= 18 else False,
        'birth_year': date.today().year - user_age
    }

    return user_dict


def generate_10_elements_list_of_dict_users_data(female_fnames_list: list, male_fnames_list: list, lnames_list: list, countries_list: list) -> list:
    list_of_users_dict = []
    for i in range(5):
        random_male_user = generate_dict_of_user_data(male_fnames_list, lnames_list, countries_list)
        random_female_user = generate_dict_of_user_data(female_fnames_list, lnames_list, countries_list)
        list_of_users_dict.extend([random_male_user, random_female_user])

    return list_of_users_dict


if __name__ == '__main__':
    # Lists with data for the draw
    female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
    female_fnames_with_error_type = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 16]
    male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

    users_dict = generate_10_elements_list_of_dict_users_data(female_fnames, male_fnames, surnames, countries)

    for user in users_dict:
        print(f"Hi! I'm {user['firstname']} {user['lastname']}. I come from {user['country']} and I was born in {user['birth_year']}")

